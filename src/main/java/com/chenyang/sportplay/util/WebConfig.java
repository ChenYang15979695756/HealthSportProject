package com.chenyang.sportplay.util;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: 陈洋
 * @Date: 2021/08/13/20:12
 * @version: 1.0
 * @注释
 */
//全局配置类--配置跨域请求
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        /**
         * 1.域访问路径
         * 2.请求来源
         * 3.方法
         * 4.允许携带
         * 5.响应时间
         */

        registry.addMapping("/**")
                .allowedOrigins("Http://localhost:8080",null)
                .allowedMethods("GET","POST","PUT","OPTION","DELETE")
                .allowCredentials(true)
                .maxAge(3600);
    }
}
